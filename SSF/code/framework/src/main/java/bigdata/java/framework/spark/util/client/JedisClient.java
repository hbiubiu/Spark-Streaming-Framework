package bigdata.java.framework.spark.util.client;

import bigdata.java.framework.spark.pool.redis.JedisConfig;
import bigdata.java.framework.spark.pool.redis.JedisConnectionPool;
import redis.clients.jedis.Jedis;

/**
 * redis客户端工具类
 */
public class JedisClient {

    private static JedisClient instance=null;
    /**
     * 获取工具类实例对象
     */
    public static JedisClient getInstance(){
        if(instance == null){
            synchronized(JedisClient.class){
                if(instance==null){
                    instance = new JedisClient();
                }
            }
        }
        return instance;
    }

    private JedisConnectionPool jedisConnectionPool = null;

    public JedisConnectionPool getConnectionPool() {
        return jedisConnectionPool;
    }

    private JedisClient() {
        JedisConfig jedisConfig = new JedisConfig();
        jedisConnectionPool = new JedisConnectionPool(jedisConfig);
    }

    /**
     * 归还链接
     * @param pool 连接池对象
     * @param resource 连接对象
     */
    public static void returnConnection(JedisConnectionPool pool, Jedis resource){
        if(pool!=null && resource!=null)
        {
            pool.returnConnection(resource);
        }
    }

    /**
     * 根据key获取value
     * @param key key 字符串
     * @return 字符串
     */
    public String get(String key)
    {
        String value=null;
        JedisConnectionPool pool = null;
        Jedis resource = null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            value = resource.get(key);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return value;
    }

    /**
     * 根据key 设置对应的value值
     * @param key key 字符串
     * @param value value 字符串
     */
    public String set(String key,String value)
    {
        JedisConnectionPool pool = null;
        Jedis resource = null;
        String returnValue=null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            returnValue = resource.set(key,value);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return returnValue;
    }
}

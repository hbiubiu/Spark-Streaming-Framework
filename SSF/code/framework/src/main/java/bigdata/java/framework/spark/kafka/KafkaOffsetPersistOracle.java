/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import bigdata.java.framework.spark.util.OracleUtil;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.spark.streaming.kafka010.OffsetRange;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class KafkaOffsetPersistOracle extends KafkaOffsetPersist {
    FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");
    String tableName ="KAFKAOFFSET";
    public KafkaOffsetPersistOracle(String appName, String topic, String groupId) {
        super(appName, topic, groupId);
        String sql = "select count(1) as c from user_tables where table_name = upper('"+tableName+"') ";
        Boolean tableExists = OracleUtil.getInstance().tableExists(tableName);
        if(!tableExists)
        {
            String createSql = "CREATE TABLE "+tableName+" (APPNAME VARCHAR2(100) NOT NULL , TOPIC VARCHAR2(100) , GROUPID VARCHAR2(100) , PARTITION INT , LASTESTOFFSET VARCHAR2(100),UPTIME VARCHAR2(100) NOT NULL)";
            OracleUtil.getInstance().ExecuteSQL(createSql);
        }
    }

    @Override
    public void clearPersist() {
        String deleteSql = "DELETE FROM " + tableName + " WHERE APPNAME='"+appName+"'";
        OracleUtil.getInstance().ExecuteSQL(deleteSql);
    }


    @Override
    public boolean persistOffset(OffsetRange[] offsetRanges) {
        Date date = new Date();
        List<String> sqlList = new ArrayList<>();
        String deleteSql = "DELETE FROM " + tableName + " WHERE APPNAME='"+appName+"'";
        sqlList.add(deleteSql);
        for (int i = 0; i < offsetRanges.length; i++) {
            OffsetRange offsetRange = offsetRanges[i];
            String sql = "INSERT INTO "+tableName + " VALUES('"+appName+"','"+topic+"','"+groupId+"',"+offsetRange.partition()+","+offsetRange.untilOffset()+",'"+fastDateFormat.format(date)+"')" ;
            sqlList.add(sql);
        }
        return OracleUtil.getInstance().ExecuteSqlList(sqlList);
    }

    @Override
    public List<TopicGroupPartitionOffset> getOffset() {
        List<TopicGroupPartitionOffset> groupTopicPartitionOffsets = new ArrayList<>();
        List<Map<String, Object>> list = OracleUtil.getInstance().QuerySQL("SELECT * FROM "+ tableName + " WHERE APPNAME='"+ appName +"'");

        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = list.get(i);
            TopicGroupPartitionOffset tgpo = new TopicGroupPartitionOffset();
            tgpo.setGroup(groupId);
            tgpo.setTopic(topic);
            Integer partition =Integer.valueOf(map.get("PARTITION").toString());
            Long lastestoffset = Long.valueOf(map.get("LASTESTOFFSET").toString()) ;
            tgpo.setPartition(partition);
            tgpo.setLastestOffset(lastestoffset);
            groupTopicPartitionOffsets.add(tgpo);
        }
        return groupTopicPartitionOffsets;
    }
}

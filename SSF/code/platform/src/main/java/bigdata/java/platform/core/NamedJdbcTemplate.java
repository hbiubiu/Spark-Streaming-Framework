package bigdata.java.platform.core;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import javax.sql.DataSource;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class NamedJdbcTemplate extends NamedParameterJdbcTemplate {

    DataSource dataSource=null;

    @Override
    public JdbcTemplate getJdbcTemplate() {
        return new MyJdbcTemplate(this.dataSource);
    }

    public NamedJdbcTemplate(DataSource dataSource) {
        super(dataSource);
        this.dataSource=dataSource;
    }

    public NamedJdbcTemplate(JdbcOperations classicJdbcTemplate) {
        super(classicJdbcTemplate);
    }

    @Override
    public <T> T queryForObject(String sql, Map<String, ?> paramMap, RowMapper<T> rowMapper) throws DataAccessException {
        List<T> results = query(sql, paramMap, new RowMapperResultSetExtractor<T>(rowMapper, 1));
        return requiredSingleResult(results);
    }
    @Override
    public <T> T queryForObject(String sql, SqlParameterSource paramSource, Class<T> requiredType) throws DataAccessException {
        List<T> results = query(sql, paramSource, new BeanPropertyRowMapper<>(requiredType));
        return requiredSingleResult(results);
    }

    @Override
    public List<Map<String, Object>> queryForList(String sql, SqlParameterSource paramSource) throws DataAccessException {
        List<Map<String, Object>> results = query(sql, paramSource, new ColumnMapRowMapper());
        return results;
    }

    @Override
    public List<Map<String, Object>> queryForList(String sql, Map<String, ?> paramMap) throws DataAccessException {
        List<Map<String, Object>> results = query(sql, paramMap, new ColumnMapRowMapper());
        return results;
    }

    public static <T>T requiredSingleResult(Collection<T> results) throws IncorrectResultSizeDataAccessException
    {
        int size = (results != null ? results.size() : 0);
        if(size ==0)
        {
            return null;
        }
        if( results.size() > 1)
        {
            throw new IncorrectResultSizeDataAccessException(1, size);
        }
        return results.iterator().next();
    }
}

package bigdata.java.platform;

import bigdata.java.platform.beans.TTask;
import bigdata.java.platform.beans.UserInfo;
import bigdata.java.platform.core.ScheduleLock;
import bigdata.java.platform.core.ScheduledTask;
import bigdata.java.platform.service.TaskChartService;
import bigdata.java.platform.service.TaskService;
import bigdata.java.platform.service.UserService;
import bigdata.java.platform.service.impl.TaskServiceImpl;
import bigdata.java.platform.util.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import static bigdata.java.platform.util.Comm.userQuaueNameMap;

@SpringBootApplication
//@EnableScheduling
public class PlatformApplication {
    public static void main(String[] args) {
        SpringApplication.run(PlatformApplication.class, args);
    }
}
